# documentation
This package is deprecated. I split it's functionality into [document](https://github.com/fvafrCU/document) and [excerptr](https://github.com/fvafrCU/excerptr), both on CRAN now.
